<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Clients'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="clients form large-9 medium-8 columns content">
    <?= $this->Form->create($client) ?>
    <fieldset>
        <legend><?= __('Add Client') ?></legend>
        <?php
            echo $this->Form->input('unique_code');
            echo $this->Form->input('email');
            echo $this->Form->input('password');
            echo $this->Form->input('salutation');
            echo $this->Form->input('first_name');
            echo $this->Form->input('last_name');
            echo $this->Form->input('date_of_birth', ['empty' => true]);
            echo $this->Form->input('gender');
            echo $this->Form->input('mobile_number');
            echo $this->Form->input('residential_number');
            echo $this->Form->input('address_street');
            echo $this->Form->input('address_building');
            echo $this->Form->input('address_unit');
            echo $this->Form->input('address_postal_code');
            echo $this->Form->input('deleted');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
