<div id="div_actions">
    <?php 
        echo '<button class="btn btn-sm btn-primary btn_margin_right_5px" data-toggle="modal" data-target="#add_event" data-remote="'.$this->Url->build(['controller' => 'calendars', 'action' => 'add']).'">'.__('Add Event').'</button>'; 
        
        echo $this->Html->link(
            __('View All Events'), 
            [
                'controller' => 'calendars', 
                'action' => 'view'
            ], 
            [
                'class' => 'btn btn-sm btn-primary'
            ]
        );
    ?>
</div>

<div id="div_filter">
    <?php 
        echo '<div class="clearfix">';        
        echo '<label class="col-md-3" for="content">Filter by user</label>';                                       
        echo '<div class="col-md-6">';
        echo $this->Form->input(null, [
            'id' => 'sel_filter_by_user',
            'options' => $staff_list,
            'value' => $user_id,
            'empty' => '('.__('all').')'
        ]);                  
        echo '</div>';
        
        echo '<div class="col-md-2">';
        echo $this->Form->button(__('Filter User'), [
            'id' => 'btn_filter_by_user', 
            'class' => 'btn btn-xs btn-primary'
        ]);  
        echo '</div>';
        echo '</div>';                         
    ?>
</div>

<div id="div_calendar"></div>

<div id="add_event" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
               <!-- remote content will be inserted here via jQuery load() -->
            </div>
        </div>
    </div>
</div>

<div id="edit_event" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
               <!-- remote content will be inserted here via jQuery load() -->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {        
        $('#div_calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'year,month,agendaWeek,agendaDay'
            },
            editable: false,
            droppable: false,
            defaultView: 'month',            
            <?php if (!empty($calendars)) { ?>
            events: [
                <?php 
                foreach ($calendars as $calendar) {               
                ?>                   
                {
                    id: <?=$calendar->id?>,
                    title: '<?=$calendar->title?>\n' + 
                        '<?=!empty($calendar->description) ? '('.$calendar->description.')' : ''?>',
                    staff: 
                    '<?php
                        if (!empty($calendar->users)) {
                            $count = 1;
                            foreach($calendar->users as $staff) {
                                echo $count.'. '.$staff->first_name.' '.$staff->last_name.' ('.$staff->email.')<br />';
                                $count++;
                            }
                        }
                    ?>',                    
                    start: '<?=$calendar->start_date?>',
                    end: '<?=$calendar->end_date?>',
                    className: ['fc-clickable'],
                    <?=(!empty($calendar->repeating) ? 'dow: ['.$calendar->repeating.'],' : '')?>
                    <?php 
                        if (!empty($calendar->leave_type['color_code'])) {
                            $event_color = $calendar->leave_type['color_code'];
                        } 
                        else {
                            $event_color = '#5bc0de';
                        }
                    ?>                    
                    color: '<?=$event_color?>',                                        
                },    
                <?php
                }
                ?>  
            ],            
            <?php } ?>
            eventRender: function(event, element) { 
                element.find('.fc-title').append('<hr style="margin: 5px 0px;" />' + event.staff); 
            }, 
            eventClick: function(event) {
                var modal = $("#edit_event");
                modal.modal({remote: '<?=$this->Url->build(['controller' => 'calendars', 'action' => 'edit'])?>/' + event.id });;
            },
        });
    
        var filter_by_user = function(e){
            e.preventDefault();             
            var user_id = $('#sel_filter_by_user').val();
            
            if (user_id !== "") {               
                window.location.href = '<?=$this->Url->build(['controller' => 'calendars', 'action' => 'by_user']);?>' + '/' + user_id;
            } 
            else {
                window.location.href = '<?=$this->Url->build(['controller' => 'calendars', 'action' => 'index']);?>';
            }
        }; 
        
        $('#btn_filter_by_user').on('click', filter_by_user);
        
    }); 
</script>
