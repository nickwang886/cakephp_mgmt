<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserLeavesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserLeavesTable Test Case
 */
class UserLeavesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserLeavesTable
     */
    public $UserLeaves;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_leaves',
        'app.users',
        'app.calendars',
        'app.leave_types',
        'app.calendars_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserLeaves') ? [] : ['className' => 'App\Model\Table\UserLeavesTable'];
        $this->UserLeaves = TableRegistry::get('UserLeaves', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserLeaves);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
