<div class="users form">
    <?= $this->Form->create($user, ['templates' => $template_form_48]) ?>
    <div class="modal-header modal-header-primary">       
        <button class="close" data-dismiss="modal">&times;</button>
        <b><?php echo __('Add User'); ?></b>
    </div>

    <div class="modal-body">
        <fieldset>
            <?php
            echo $this->Form->input('email');
            echo $this->Form->input('password');
            echo $this->Form->input('first_name');
            echo $this->Form->input('last_name');
            echo $this->Form->input('contact_number');
            echo $this->Form->input('access_level', [
                'options' => [
                    2 => 'System Admin',
                    3 => 'Staff'
                ],
                'default' => 3
            ]);

            // leave balance
            echo '<hr style="border-top: 1px dotted #000;" />';

            echo '<div class="col-md-4"></div>'
            . '<div class="col-md-8"><small class="custom_info_message">&#x26a0; ' . __('Leave blank if not applicable') . '</small></div>';

            if (!empty($leave_types)) {
                foreach ($leave_types as $leave_type) {
                    echo $this->Form->input('user_leaves[' . $leave_type->id . ']', [
                        'label' => $leave_type->name
                    ]);
                }
            }
            ?>
        </fieldset>
    </div>

    <div class="modal-footer">
        <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary pull-right']) ?>
        <button class="btn btn-default btn_margin_right_5px" data-dismiss="modal"><?= __('Close') ?></button>
        <?= $this->Form->end() ?> 
    </div>
</div>
