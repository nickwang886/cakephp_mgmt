<?php
namespace App\View\Cell;

use Cake\View\Cell;

/**
 * GetAccessLevelName cell
 */
class GetAccessLevelNameCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($access_level_id = null)
    {
        switch ($access_level_id) {
            case 1:
                $access_level_name = 'Super Admin';
                break;
            case 2:
                $access_level_name = 'System Admin';
                break;
            case 3:
                $access_level_name = 'Staff';
                break;
            default:
                $access_level_name = 'Unknown';
                break;
        }

        $this->set('access_level_name', $access_level_name);
    }
}
