<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Dashboard Controller
 *
 * @property \App\Model\Table\DashboardTable $Dashboard
 */
class DashboardController extends AppController
{
    var $uses = false;

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {        
        // super user and admin access only
        $session = $this->request->session();
        if (($session->read('user_type') != 'user')) {       
            $this->Flash->set(
                __('Access Denied!'), 
                ['params' => 
                    [
                        'class' => 'alert alert-danger'
                    ]
                ]
            );
            $this->redirect(['controller' => 'dashboard', 'action' => 'client']);
        }
        // end of super user and admin access check
    }
           
    /**
     * client index
     *
     * @return \Cake\Network\Response|null
     */
    public function client()
    {
        $this->viewBuilder()->layout('client');
    }
}
