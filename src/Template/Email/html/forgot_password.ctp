<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>       
        Dear <?=(!empty($name) ? $name : 'user')?>,<br />
        <p>You recently requested a password reset.</p>
        <p>Below is your password reset link: <br /><a href="<?=$url?>"><?=$url?></a></p>
        <hr />
        <p>If you did not initiate this request, please kindly ignore this email.</p>                
    </body>
</html>