<div class="users form">
    <?= $this->Form->create($user, ['templates' => $template_form_48]) ?>
    <div class="modal-header modal-header-primary">       
        <button class="close" data-dismiss="modal">&times;</button>
        <b><?php echo __('Edit User'); ?></b>
    </div>

    <div class="modal-body">
        <fieldset>
            <?php
            echo $this->Form->input('email');
            //echo $this->Form->input('password');
            echo $this->Form->input('first_name');
            echo $this->Form->input('last_name');
            echo $this->Form->input('contact_number');
            
            if ($this->request->session()->read('Auth.User.access_level') < 3) {
                echo $this->Form->input('access_level', [
                    'options' => [
                        2 => 'System Admin',
                        3 => 'Staff'
                    ],
                    'value' => $user->access_level
                ]);
            }
            
            if ($this->request->session()->read('Auth.User.access_level') < 3) {
                echo '<hr style="border-top: 1px dotted #000;" />';

                echo '<div class="col-md-4"></div>'
                . '<div class="col-md-8"><small class="custom_info_message">&#x26a0; ' . __('Leave blank if not applicable') . '</small></div>';

                // build an array for all exiting user leave types
                $array_user_leave_types = array();

                // existing user leave balance
                if (!empty($user->user_leaves)) {
                    foreach ($user->user_leaves as $user_leave) {
                        echo $this->Form->input('user_leaves[' . $user_leave->leave_type_id . ']', [
                            'label' => $user_leave->leave_type['name'],
                            'value' => $user_leave->balance
                        ]);

                        $array_user_leave_types[] = $user_leave->leave_type_id;
                    }
                }

                // new user leave balance
                if (!empty($leave_types)) {
                    foreach ($leave_types as $leave_type) {
                        if (!in_array($leave_type->id, $array_user_leave_types)) {
                            echo $this->Form->input('user_leaves[' . $leave_type->id . ']', [
                                'label' => $leave_type->name
                            ]);
                        }
                    }
                }
            }
            ?>
        </fieldset>
    </div>

    <div class="modal-footer">
        <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary pull-right']) ?>
        <button class="btn btn-default btn_margin_right_5px" data-dismiss="modal"><?= __('Close') ?></button>
        <?= $this->Form->end() ?> 
    </div>
</div>
