<div class="users form col-md-8 col-md-offset-2 columns content">
    <?= $this->Form->create($user, ['templates' => $template_form_48]) ?>
    <fieldset>
        <?php            
            echo $this->Form->input('password', [
                'label' => __('New Password'),
                'value' => ''
            ]);
            echo $this->Form->input('confirm_password', [
                'label' => __('Confirm Password'),
                'type' => 'password',
                'value' => ''                
            ]);
        ?>
    
        <div class="col-md-12">
            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary pull-right']) ?>
            <?= $this->Form->end() ?>
        </div>  
    </fieldset>
</div>
