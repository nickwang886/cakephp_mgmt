<div id="div_actions">
    <?php
    echo '<button class="btn btn-sm btn-primary btn_margin_right_5px" data-toggle="modal" data-target="#add_event" data-remote="' . $this->Url->build(['controller' => 'calendars', 'action' => 'add']) . '">' . __('Leave Application') . '</button>';

    echo $this->Html->link(
            __('Calendar Mode'), [
        'controller' => 'calendars',
        'action' => 'index'
            ], [
        'class' => 'btn btn-sm btn-info'
            ]
    );
    ?>
</div>

<div id="div_data_table">    
    <table id="tbl_calendars" class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th><?= __('Start') ?></th>
                <th><?= __('End') ?></th>
                <th><?= __('Type') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Staff') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($calendars as $calendar): ?>
                <tr>
                    <td><?= $calendar->start_date ?></td>
                    <td><?= $calendar->end_date ?></td> 
                    <td><?= $calendar->leave_type['name'] ?></td>
                    <td>
                        <?php
                        // check all day
                        if ($calendar->all_day == 1) {
                            echo '<i class="fa fa-calendar-times-o text-info" aria-hidden="true"></i>&nbsp;';
                        }

                        // check repeat
                        if (!empty($calendar->repeating)) {
                            echo '<i class="fa fa-repeat text-info" aria-hidden="true"></i>&nbsp;';
                        }
                        ?>
                        <?= h($calendar->title) ?>
                    </td>
                    <td>
                        <?php
                        if (!empty($calendar->users)) {
                            foreach ($calendar->users as $user) {
                                echo $user->first_name . ' ' . $user->last_name . ' (' . $user->email . ')<br />';
                            }
                        }
                        ?>
                    </td>                
                    <td class="actions">
                        <?php
                        if ($this->request->session()->read('Auth.User.access_level') < 3) {
                            echo '<button class="btn btn-xs btn-primary btn_margin_right_5px" data-toggle="modal" data-target="#edit_event" data-remote="' . $this->Url->build(['controller' => 'calendars', 'action' => 'edit', $calendar->id]) . '">' . __('Edit') . '</button>';                                                
                            echo $this->Form->postLink(__('Delete'), ['action' => 'delete', $calendar->id], ['class' => 'btn btn-xs btn-danger', 'confirm' => __('Confirm to delete?')]);
                        }
                        ?>
                    </td>                
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div id="add_event" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <!-- remote content will be inserted here via jQuery load() -->
            </div>
        </div>
    </div>
</div>

<div id="edit_event" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <!-- remote content will be inserted here via jQuery load() -->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#tbl_calendars').DataTable({
            stateSave: true,
            order: [[0, "desc"]],
            iDisplayLength: 20,
            aLengthMenu: [[20, 50, 100, -1], [20, 50, 100, "All"]],
            responsive: true,
            dom: '<lBf<t>ip>', // B = button, l = select entries, f = search bokx, i = record info, p = pagination                      
            buttons: [
                'copy',
                {
                    extend: 'csv',
                    title: 'data_export_<?= date('Ymd') ?>'
                },
                {
                    extend: 'excel',
                    title: 'data_export_<?= date('Ymd') ?>'
                },
                {
                    extend: 'pdf',
                    title: 'data_export_<?= date('Ymd') ?>'
                },
                'print'
            ],
            columns: [
                {width: "15%"},
                {width: "15%"},
                {width: "15%"},
                {width: "25%"},
                {width: "15%"},
                {width: "15%"},
            ],
        });
    });
</script>
