<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    /**
     * Actions allowed without login
     *
     * @return void
     */
    public function beforeFilter(Event $event)
    {
        parent::initialize();
        
        $this->Auth->config([
            'authenticate' => [
                'Form' => [
                    'userModel' => 'users',
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'users',
                'action' => 'login'
            ],
            'loginRedirect' => [
                'controller' => 'dashboard',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'users',
                'action' => 'login'
            ]
        ]);
        
        $this->Auth->allow(['forgotPassword', 'resetPassword', 'setNewPassword']);                       
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // user access check        
        $session = $this->request->session();
        
        if (($session->read('user_type') != 'user')) {       
            $this->Flash->set(
                __('Access Denied!'), 
                ['params' => 
                    [
                        'class' => 'alert alert-danger'
                    ]
                ]
            );

            $this->redirect(['controller' => 'clients', 'action' => 'logout']);
        }                        
        // user access check
        
        if ($this->Auth->user('access_level') < 3) {
            // admin access
            $users = $this->Users->find('all', [
                    'contain' => ['UserLeaves']
                ])
                ->where(['Users.access_level >' => 1]);
        } 
        else {
            // staff access
            $users = $this->Users->find('all', [
                    'contain' => ['UserLeaves']
                ])
                ->where(['Users.id' => $this->Auth->user('id')])                        
                ->limit(1);
        }

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        //user access check
        if ($this->Auth->user('access_level') > 2) {
            
            $session = $this->request->session();
            
            if ($session->read('user_type') != 'user' || $this->Auth->user('id') != $id) {        
                $this->Flash->set(
                    __('Access Denied!'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-danger'
                        ]
                    ]
                );
                
                $this->redirect(['controller' => 'dashboard', 'action' => 'index']);
            }
        }
        // end of user access check
        
        $user = $this->Users->get($id, [
                'contain' => [
                    'Calendars' => 
                        [
                            'LeaveTypes'
                        ]
                    ]
                ]
            );

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
        
        // get user leaves
        $user_leaves = $this->Users->UserLeaves->find('all', [
                'contain' => ['LeaveTypes']
            ])
            ->where(['UserLeaves.user_id' => $id]);
        
        $this->set(compact('user_leaves'));
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {     
        // user access check        
        $session = $this->request->session();
        
        if (($session->read('user_type') != 'user')) {       
            $this->Flash->set(
                __('Access Denied!'), 
                ['params' => 
                    [
                        'class' => 'alert alert-danger'
                    ]
                ]
            );

            $this->redirect(['controller' => 'clients', 'action' => 'logout']);
        }                        
        // user access check
        
        $user = $this->Users->newEntity();
        
        if ($this->request->is('post')) {
            
            $user = $this->Users->patchEntity($user, $this->request->data);
            
            if ($this->Users->save($user)) {
                // user leaves
                if (!empty($this->request->data['user_leaves'])) {  
                    foreach ($this->request->data['user_leaves'] as $key => $value) {
                        if (!empty($value)) {
                            $userLeavesTable = TableRegistry::get('UserLeaves');

                            $new_user_leaves = $userLeavesTable->newEntity();
                            $new_user_leaves->user_id = $user->id;
                            $new_user_leaves->leave_type_id = $key;
                            $new_user_leaves->balance = $value;

                            $userLeavesTable->save($new_user_leaves);                         
                        }
                    }
                }
                
                //$this->Flash->success(__('The user has been saved.'));
                $this->Flash->set(
                    __('The user has been saved.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-success'
                        ]
                    ]
                );
            } 
            else {
                //$this->Flash->error(__('The user could not be saved. Please, try again.'));
                $this->Flash->set(
                    __('The user could not be saved. Please, try again.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-danger'
                        ]
                    ]
                );
            }
            
            return $this->redirect(['action' => 'index']);
        }
        
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
        
        // leave types
        $this->loadModel('LeaveTypes');
        $leave_types = $this->LeaveTypes->find();
        
        $this->set(compact('leave_types'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        //user access check
        if ($this->Auth->user('access_level') > 2) {
            
            $session = $this->request->session();
            
            if ($session->read('user_type') != 'user' || $this->Auth->user('id') != $id) {        
                $this->Flash->set(
                    __('Access Denied!'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-danger'
                        ]
                    ]
                );
                
                $this->redirect(['controller' => 'dashboard', 'action' => 'index']);
            }
        }
        // end of user access check
        
        $user = $this->Users->get($id, [
            'contain' => [
                'UserLeaves' => [
                    'LeaveTypes'
                ]
            ]
        ]);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $user = $this->Users->patchEntity($user, $this->request->data);
            
            if ($this->Users->save($user)) {                
                // user leaves
                if (!empty($this->request->data['user_leaves'])) {  
                    $this->loadModel('UserLeaves');
                    
                    foreach ($this->request->data['user_leaves'] as $key => $value) {
                        $user_leave = $this->UserLeaves->find()
                                ->where([
                                    'user_id' => $user->id,
                                    'leave_type_id' => $key
                                ])
                                ->first();
                        
                        if (!empty($user_leave)) {
                            // existing record
                            if (!empty($value)) {
                                // update record
                                $user_leave->balance = $value;

                                $this->UserLeaves->save($user_leave);
                            } 
                            else {
                                // delete record
                                $this->UserLeaves->delete($user_leave);
                            }
                        } 
                        else {
                            // new record
                            if (!empty($value)) {
                                $userLeavesTable = TableRegistry::get('UserLeaves');

                                $new_user_leaves = $userLeavesTable->newEntity();
                                $new_user_leaves->user_id = $user->id;
                                $new_user_leaves->leave_type_id = $key;
                                $new_user_leaves->balance = $value;

                                $userLeavesTable->save($new_user_leaves);                         
                            }
                        }                                                
                    }
                }
                
                //$this->Flash->success(__('The user has been saved.'));
                $this->Flash->set(
                    __('The user has been saved.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-success'
                        ]
                    ]
                );
            } 
            else {
                //$this->Flash->error(__('The user could not be saved. Please, try again.'));                
                $this->Flash->set(
                    __('The user could not be saved. Please, try again.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-danger'
                        ]
                    ]
                );
            }
            
            return $this->redirect(['action' => 'index']);
        }
        
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);     
        
        // leave types
        $this->loadModel('LeaveTypes');
        $leave_types = $this->LeaveTypes->find();
        
        $this->set(compact('leave_types'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        
        // delete all user leaves
        $this->loadModel('UserLeaves');
        $this->UserLeaves->deleteAll(['user_id' => $id]);
                
        if ($this->Users->delete($user)) {
            //$this->Flash->success(__('The user has been deleted.'));
            $this->Flash->set(
                __('The user has been deleted.'), 
                ['params' => 
                    [
                        'class' => 'alert alert-success'
                    ]
                ]
            );
        } 
        else {
            //$this->Flash->error(__('The user could not be deleted. Please, try again.'));
            $this->Flash->set(
                __('The user could not be deleted. Please, try again.'), 
                ['params' => 
                    [
                        'class' => 'alert alert-success'
                    ]
                ]
            );
        }
        
        return $this->redirect(['action' => 'index']);
    }
    
    /**
     * login
     * @return type
     */
    public function login() {
        //$this->viewBuilder()->layout(false);
        
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            
            if ($user) {
                $this->Auth->setUser($user);
                
                // store session user type
                $session = $this->request->session();
                $session->write('user_type', 'user');
                
                return $this->redirect($this->Auth->redirectUrl());
            }
            
            // user not identified
            $this->Flash->set(
                __('Your username or password is incorrect'), 
                ['params' => 
                    [
                        'class' => 'alert alert-danger'
                    ]
                ]
            );
        }
    }
    
    /**
     * logout
     * @return type
     */
    public function logout() {        
        // destory session
        $session = $this->request->session();
        $session->delete('user_type');
        
        //$this->Flash->success['You are now logged out'];
        $this->Flash->set(
            __('You are now logged out.'), 
            ['params' => 
                [
                    'class' => 'alert alert-info'
                ]
            ]
        );
        
        return $this->redirect(($this->Auth->logout()));
    }
    
    /**
     * forgot password
     * @return type
     */    
    public function forgotPassword() {
        if ($this->request->is('post')) {
            $update_password_url = null;

            $user_email = $this->request->data['email'];
            $user = $this->Users->find()
                    ->where(['Users.email' => $user_email])
                    ->first();

            if (!empty($user)) {
                $token = $user->id.'__'.strtotime($user->created).'__'.time();
                $hashed_token = base64_encode($token);
                $update_password_url = Router::url('/', true)."users/reset-password?token=".$hashed_token;
                
                $email = new Email();
                $email->template('forgot_password')
                    ->emailFormat('html')                    
                    ->to($user_email)
                    ->from('reset-password@system.com')
                    ->subject('Password reset')
                    ->viewVars([ 
                        'url' => $update_password_url
                    ])                    
                    ->send();               
            } 
            
            $this->Flash->set(
                __('Reset password email has been sent to the entered email address.'), 
                ['params' => 
                    [
                        'class' => 'alert alert-info'
                    ]
                ]
            );
            
            $this->redirect(['action' => 'login']);
        }
    }
    
    /**
     * reset password
     * @return type
     */    
    public function resetPassword($user_id = null) {
        
        $this->autoRender = false;
        
        if ($this->request->is('get')) {
            if (empty($this->request->query('token'))){
                return $this->redirect(['action' => 'forgotPassword']);
            } 
            else {
                $hashed_token = $this->request->query('token');                                
                
                if (!empty($hashed_token)) {
                    return $this->redirect(['action' => 'setNewPassword', $hashed_token]);
                } 
                else {
                    $this->Flash->set(
                        __('Invalid token'), 
                        ['params' => 
                            [
                                'class' => 'alert alert-danger'
                            ]
                        ]
                    );

                    return $this->redirect(['action' => 'forgotPassword']);
                }                                                     
            }
        }                
    }
    
    /**
     * set new password
     * @return type
     */
    public function setNewPassword($hashed_token = null)
    {
        $decode_token = base64_decode($hashed_token);
                
        $data = explode('__', $decode_token);

        $user_id = $data[0];
        $user_created = $data[1];                
        $submission_datetime = $data[2];
        
        // compare time stamp
        $cur_datetime = time();
        
        $user = $this->Users->get($user_id);
        
        // check if created timestamp matches
        if (empty($user_id) || (strtotime($user->created) != $user_created)) { 
            $this->Flash->set(
                __('Invalid token'), 
                ['params' => 
                    [
                        'class' => 'alert alert-danger'
                    ]
                ]
            );
            
            return $this->redirect(['action' => 'login']);
        } 
        
        // check if token expired
        if (empty($submission_datetime) || (($cur_datetime - $submission_datetime) > 60 * 60 * 24)) {  // 24 hours             
            $this->Flash->set(
                __('Expired token'), 
                ['params' => 
                    [
                        'class' => 'alert alert-danger'
                    ]
                ]
            ); 
            
            return $this->redirect(['action' => 'login']);
        }
                                                
        if ($this->request->is(['patch', 'post', 'put'])) {
                         
            $user = $this->Users->patchEntity($user, $this->request->data);

            if ($this->Users->save($user)) {
                //$this->Flash->success(__('The password has been saved.'));
                $this->Flash->set(
                    __('The password has been saved.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-success'
                        ]
                    ]
                );

                return $this->redirect(['action' => 'login']);
            } 
            else {
                //$this->Flash->error(__('The password could not be saved. Please, try again.'));
                $this->Flash->set(
                    __('The password could not be saved. Please, try again.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-danger'
                        ]
                    ]
                );
            }             
        }
        
        $this->set(compact('user'));
    }
            
    /**
     * update password
     * @return type
     */
    public function updatePassword($id = null)
    {
        // user access check
        $session = $this->request->session();
        
        if ($session->read('user_type') != 'user' || $this->Auth->user('id') != $id) {       
            $this->Flash->set(
                __('Access Denied!'), 
                ['params' => 
                    [
                        'class' => 'alert alert-danger'
                    ]
                ]
            );
            
            $this->redirect(['controller' => 'dashboard', 'action' => 'index']);
        }
        // end of user access check
        
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            //$user = $this->Users->patchEntity($user, $this->request->data);
            
            $user = $this->Users->patchEntity($user, [
                    'old_password' => $this->request->data['old_password'],
                    'password' => $this->request->data['password'],
                    'confirm_password' => $this->request->data['confirm_password']
                ]
            );
            
            if ($this->Users->save($user)) {
                //$this->Flash->success(__('The password has been saved.'));
                $this->Flash->set(
                    __('The password has been saved.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-success'
                        ]
                    ]
                );
                //return $this->redirect(['action' => 'index']);
            } 
            else {
                //$this->Flash->error(__('The password could not be saved. Please, try again.'));
                $this->Flash->set(
                    __('The password could not be saved. Please, try again.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-danger'
                        ]
                    ]
                );
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);        
    }    
    
}
