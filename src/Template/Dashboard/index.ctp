<div id="div_actions">
    <h3 class="text-center"><i class="fa fa-cog fa-fw"></i> <?=__('Modules')?></h3>
    <hr />    
    <div class="col-md-4 text-center div_actions_items">
        <?php
            echo $this->Html->link(
                '<i class="fa fa-user fa-4x" aria-hidden="true"></i><br />'.
                ($this->request->session()->read('Auth.User.access_level') < 3 ? __('Admin') : __('Profile')), 
                [
                    'controller' => 'users', 
                    'action' => 'index'
                ], 
                [
                    'escape' => false
                ]
            );          
        ?>
    </div>
    <div class="col-md-4 text-center div_actions_items">
        <?php
            echo $this->Html->link(
                '<i class="fa fa-calendar fa-4x" aria-hidden="true"></i><br />'.__('Leave'), 
                [
                    'controller' => 'calendars', 
                    'action' => 'index'
                ], 
                [
                    'escape' => false
                ]
            );          
        ?>
    </div>    
</div>

<style>
    .div_actions_items {
        margin: 10px 0px;
    }
    
    .div_actions_items a:hover {
        text-decoration: none;
    }
</style>
