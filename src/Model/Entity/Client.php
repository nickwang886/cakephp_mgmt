<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Client Entity.
 *
 * @property int $id
 * @property string $unique_code
 * @property string $email
 * @property string $password
 * @property string $salutation
 * @property string $first_name
 * @property string $last_name
 * @property \Cake\I18n\Time $date_of_birth
 * @property string $gender
 * @property string $mobile_number
 * @property string $residential_number
 * @property string $address_street
 * @property string $address_building
 * @property string $address_unit
 * @property string $address_postal_code
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $deleted
 */
class Client extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    /**
     * Fields that are excluded from JSON an array versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
    
    protected function _setPassword($value) {
        $hasher = new \Cake\Auth\DefaultPasswordHasher;
        return $hasher->hash($value);
    }
}
