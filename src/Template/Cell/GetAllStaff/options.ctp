<?php
    // staff    
    if (!empty($staff)) {
        echo '<div class="form-group clearfix option_staff_list">';
        echo '<label class="col-md-4 control-label" for="title">'.__('Staff').'</label>';
        echo '<div class="col-md-8">';            
        foreach ($staff as $staff) {
            echo '<div class="col-md-6" style="padding-left: 0px;">'
            . '<input type="checkbox" name="staff_ids[]" value="'.$staff->id.'"> '.$staff->first_name.' '.$staff->last_name.' ('.$staff->email.')</div>';
        }            
        echo '</div>';
        echo '</div>';
    }

