<div class="calendars form">
    <?php
    echo $this->Form->create($calendar, [
        'templates' => $template_form_48
    ]);
    ?>

    <div class="modal-header modal-header-primary">       
        <button class="close" data-dismiss="modal">&times;</button>
        <b><?php echo __('Leave Application'); ?></b>
    </div>

    <div class="modal-body">
        <fieldset>
            <?php
            if ($this->request->session()->read('Auth.User.access_level') < 3) {
                // admin
                echo $this->Form->input('leave_type_id', [
                    'options' => $leave_types
                ]);
            } 
            else {
                // staff
                $options_user_leave_types = array();
                if (!empty($user->user_leaves)) {
                    foreach ($user->user_leaves as $user_leave) {
                        $options_user_leave_types[$user_leave->leave_type_id] = $user_leave->leave_type['name'] . ' (Balance = ' . $user_leave->balance . ')';
                    }
                }

                echo $this->Form->input('leave_type_id', [
                    'options' => $options_user_leave_types
                ]);
            }

            echo $this->Form->input('title', [
                'label' => 'Duration',
                'options' => [
                    'full_day' => 'Full-day',
                    'half_day_am' => 'Half-day (AM)',
                    'half_day_pm' => 'Half-day (PM)'
                ]
            ]);

            // description
            echo '<div class="form-group clearfix">';
            echo '<label class="col-md-4 control-label">' . __('Description') . '</label>';
            echo '<div class="col-md-8">';
            echo $this->Form->input('description', [
                'label' => false,
                'type' => 'textarea',
                'class' => 'form-control',
                'style' => 'min-width: 100%',
                'rows' => 3
            ]);
            echo '</div>';
            echo '</div>';

            // start date
            echo '<div class="form-group clearfix required">';
            echo '<label class="col-md-4 control-label">' . __('Start Date') . '</label>';
            echo '<div class="col-md-8">';
            echo '<div class="input-group date" id="start_date">';
            echo '<input type="text" name="start_date" class="input-sm form-control" value="' . date('Y-m-d') . '" />';
            echo '<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>';
            echo '</div>';
            echo '</div>';
            echo '</div>';

            // end date
            echo '<div class="form-group clearfix required">';
            echo '<label class="col-md-4 control-label">' . __('End Date') . '</label>';
            echo '<div class="col-md-8">';
            echo '<div class="input-group date" id="end_date">';
            echo '<input type="text" name="end_date" class="input-sm form-control" value="' . date('Y-m-d') . '" />';
            echo '<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>';
            echo '</div>';
            echo '</div>';
            echo '</div>';

            echo '<div class="col-md-4"></div>'
            . '<div class="col-md-8"><small class="custom_info_message">&#x26a0; ' . __('Full-day') . '  = 1, ' . __('Half-day') . '  = 0.5</small></div>';

            echo $this->Form->input('total_days', [
                'min' => 0
            ]);

            if ($this->request->session()->read('Auth.User.access_level') < 3) {
                // staff options
                echo $this->cell('GetAllStaff::options');
            } else {
                // user
                echo $this->Form->hidden('staff_ids[]', [
                    'value' => $this->request->session()->read('Auth.User.id')
                ]);
            }
            ?>
        </fieldset>
    </div>

    <div class="modal-footer">
        <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary pull-right']) ?>
        <button class="btn btn-default btn_margin_right_5px" data-dismiss="modal"><?= __('Close') ?></button>
        <?= $this->Form->end() ?> 
    </div>
</div>

<style>
    .option_staff_list {        
        max-height: 200px;
        overflow: auto;
    }
</style>

<script type="text/javascript">
    $(function () {
        $('#start_date, #end_date').datetimepicker({
            format: 'YYYY-MM-DD'
        });
    });
</script>
