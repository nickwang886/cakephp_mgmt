<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UserLeaves Controller
 *
 * @property \App\Model\Table\UserLeavesTable $UserLeaves
 */
class UserLeavesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'LeaveTypes']
        ];
        $userLeaves = $this->paginate($this->UserLeaves);

        $this->set(compact('userLeaves'));
        $this->set('_serialize', ['userLeaves']);
    }

    /**
     * View method
     *
     * @param string|null $id User Leave id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userLeave = $this->UserLeaves->get($id, [
            'contain' => ['Users', 'LeaveTypes']
        ]);

        $this->set('userLeave', $userLeave);
        $this->set('_serialize', ['userLeave']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userLeave = $this->UserLeaves->newEntity();
        if ($this->request->is('post')) {
            $userLeave = $this->UserLeaves->patchEntity($userLeave, $this->request->data);
            if ($this->UserLeaves->save($userLeave)) {
                $this->Flash->success(__('The user leave has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user leave could not be saved. Please, try again.'));
            }
        }
        $users = $this->UserLeaves->Users->find('list', ['limit' => 200]);
        $leaveTypes = $this->UserLeaves->LeaveTypes->find('list', ['limit' => 200]);
        $this->set(compact('userLeave', 'users', 'leaveTypes'));
        $this->set('_serialize', ['userLeave']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User Leave id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userLeave = $this->UserLeaves->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userLeave = $this->UserLeaves->patchEntity($userLeave, $this->request->data);
            if ($this->UserLeaves->save($userLeave)) {
                $this->Flash->success(__('The user leave has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user leave could not be saved. Please, try again.'));
            }
        }
        $users = $this->UserLeaves->Users->find('list', ['limit' => 200]);
        $leaveTypes = $this->UserLeaves->LeaveTypes->find('list', ['limit' => 200]);
        $this->set(compact('userLeave', 'users', 'leaveTypes'));
        $this->set('_serialize', ['userLeave']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User Leave id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userLeave = $this->UserLeaves->get($id);
        if ($this->UserLeaves->delete($userLeave)) {
            $this->Flash->success(__('The user leave has been deleted.'));
        } else {
            $this->Flash->error(__('The user leave could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
