<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CalendarsUsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CalendarsUsersTable Test Case
 */
class CalendarsUsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CalendarsUsersTable
     */
    public $CalendarsUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.calendars_users',
        'app.calendars',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CalendarsUsers') ? [] : ['className' => 'App\Model\Table\CalendarsUsersTable'];
        $this->CalendarsUsers = TableRegistry::get('CalendarsUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CalendarsUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
