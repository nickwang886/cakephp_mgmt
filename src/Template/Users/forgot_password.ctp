<div class="users form col-md-8 col-md-offset-2 columns content">
    <?php echo $this->Form->create(null, ['templates' => $template_form_48]); ?>
    <legend class="text-center"><?=__('Please enter your registered email address')?></legend>
    <fieldset>        
        <?php
            echo $this->Form->input('email', [
                'required' => true
            ]);
        ?>
        <div class="col-md-12">
        <?php
            echo $this->Form->button(__('Reset Password'), [
                'class' => 'btn btn-primary pull-right'
            ]);      
            echo $this->Html->link(__('Cancel'), [
                'controller' => 'users', 'action' => 'login'
            ], [
                'class' => 'btn btn-default pull-right btn_margin_right_5px'
            ]);
            echo $this->Form->end(); 
        ?>
        </div>
    </fieldset>
</div>