<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use Cake\Event\Event;

/**
 * Clients Controller
 *
 * @property \App\Model\Table\ClientsTable $Clients
 */
class ClientsController extends AppController
{
    
    /**
     * Actions allowed without login
     *
     * @return void
     */
    public function beforeFilter(Event $event)
    {
        parent::initialize();
        
        $this->Auth->config([
            'authenticate' => [
                'Form' => [
                    'userModel' => 'clients',
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'clients',
                'action' => 'login'
            ],
            'loginRedirect' => [
                'controller' => 'dashboard',
                'action' => 'client'
            ],
            'logoutRedirect' => [
                'controller' => 'clients',
                'action' => 'login'
            ]
        ]);
        
        $this->Auth->allow(['add', 'forgotPassword', 'resetPassword', 'setNewPassword']);
        
        // set default layout to client
        $this->viewBuilder()->layout('client');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $clients = $this->paginate($this->Clients);

        $this->set(compact('clients'));
        $this->set('_serialize', ['clients']);
    }

    /**
     * View method
     *
     * @param string|null $id Client id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $client = $this->Clients->get($id, [
            'contain' => []
        ]);

        $this->set('client', $client);
        $this->set('_serialize', ['client']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $client = $this->Clients->newEntity();
        if ($this->request->is('post')) {
            $client = $this->Clients->patchEntity($client, $this->request->data);
            if ($this->Clients->save($client)) {
                $this->Flash->success(__('The client has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The client could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('client'));
        $this->set('_serialize', ['client']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Client id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        //user access check
        $session = $this->request->session();
        if ($session->read('user_type') != 'client' || $this->Auth->user('id') != $id) {       
            $this->Flash->set(
                __('Access Denied!'), 
                ['params' => 
                    [
                        'class' => 'alert alert-danger'
                    ]
                ]
            );
            $this->redirect(['controller' => 'dashboard', 'action' => 'client']);
        }
        // end of user access check
        
        $client = $this->Clients->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $client = $this->Clients->patchEntity($client, $this->request->data);
            if ($this->Clients->save($client)) {
                $this->Flash->success(__('The client has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The client could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('client'));
        $this->set('_serialize', ['client']);        
    }

    /**
     * Delete method
     *
     * @param string|null $id Client id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $client = $this->Clients->get($id);
        if ($this->Clients->delete($client)) {
            $this->Flash->success(__('The client has been deleted.'));
        } else {
            $this->Flash->error(__('The client could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    /**
     * login
     * @return type
     */
    public function login() {        
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            
            if ($user) {
                $this->Auth->setUser($user);
                
                // store session user type
                $session = $this->request->session();
                $session->write('user_type', 'client');
                
                return $this->redirect($this->Auth->redirectUrl());
            }
            
            // user not identified
            $this->Flash->set(
                __('Your username or password is incorrect'), 
                ['params' => 
                    [
                        'class' => 'alert alert-danger'
                    ]
                ]
            );
        }
    }
    
    /**
     * logout
     * @return type
     */
    public function logout() {
        
        // destory session
        $session = $this->request->session();
        $session->delete('user_type');
        
        //$this->Flash->success['You are now logged out'];
        $this->Flash->set(
            __('You are now logged out.'), 
            ['params' => 
                [
                    'class' => 'alert alert-info'
                ]
            ]
        );
        return $this->redirect(($this->Auth->logout()));
    }
    
    /**
     * forgot password
     * @return type
     */    
    public function forgotPassword() {
        if ($this->request->is('post')) {
            $update_password_url = null;

            $user_email = $this->request->data['email'];
            $user = $this->Clients->find()
                    ->where(['Clients.email' => $user_email])
                    ->first();

            if (!empty($user)) {
                $token = $user->id.'__'.strtotime($user->created).'__'.time();
                $hashed_token = base64_encode($token);
                $update_password_url = Router::url('/', true)."clients/reset-password?token=".$hashed_token;
                $user_name = $user->first_name.' '. $user->last_name;

                $email = new Email();
                $email->template('forgot_password')
                    ->emailFormat('html')                    
                    ->to($user_email)
                    ->from('reset-password@system.com')
                    ->subject('Password reset')
                    ->viewVars([
                        'name' => $user_name, 
                        'url' => $update_password_url
                    ])                    
                    ->send();               
            } 
            
            $this->Flash->set(
                __('Reset password email has been sent to the entered email address.'), 
                ['params' => 
                    [
                        'class' => 'alert alert-info'
                    ]
                ]
            );
            
            $this->redirect(['action' => 'login']);
        }
    }
    
    /**
     * reset password
     * @return type
     */    
    public function resetPassword($user_id = null) {
        $this->autoRender = false;
        
        if ($this->request->is('get')) {
            if (empty($this->request->query('token'))){
                return $this->redirect(['action' => 'forgotPassword']);
            } 
            else {
                $hashed_token = $this->request->query('token');                                
                
                if (!empty($hashed_token)) {
                    return $this->redirect(['action' => 'setNewPassword', $hashed_token]);
                } 
                else {
                    $this->Flash->set(
                        __('Invalid token'), 
                        ['params' => 
                            [
                                'class' => 'alert alert-danger'
                            ]
                        ]
                    );

                    return $this->redirect(['action' => 'forgotPassword']);
                }                                                     
            }
        }                
    }
    
    /**
     * set new password
     * @return type
     */
    public function setNewPassword($hashed_token = null)
    {
        $decode_token = base64_decode($hashed_token);
                
        $data = explode('__', $decode_token);

        $user_id = $data[0];
        $user_created = $data[1];                
        $submission_datetime = $data[2];
        
        // compare time stamp
        $cur_datetime = time();
        
        $user = $this->Clients->get($user_id);
        
        // check if created timestamp matches
        if (empty($user_id) || (strtotime($user->created) != $user_created)) { 
            $this->Flash->set(
                __('Invalid token'), 
                ['params' => 
                    [
                        'class' => 'alert alert-danger'
                    ]
                ]
            );
            
            return $this->redirect(['action' => 'login']);
        } 
        
        // check if token expired
        if (empty($submission_datetime) || (($cur_datetime - $submission_datetime) > 60 * 60 * 24)) {  // 24 hours             
            $this->Flash->set(
                __('Expired token'), 
                ['params' => 
                    [
                        'class' => 'alert alert-danger'
                    ]
                ]
            ); 
            
            return $this->redirect(['action' => 'login']);
        }
                                                
        if ($this->request->is(['patch', 'post', 'put'])) {
                         
            $user = $this->Clients->patchEntity($user, $this->request->data);

            if ($this->Clients->save($user)) {
                //$this->Flash->success(__('The password has been saved.'));
                $this->Flash->set(
                    __('The password has been saved.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-success'
                        ]
                    ]
                );

                return $this->redirect(['action' => 'login']);
            } else {
                //$this->Flash->error(__('The password could not be saved. Please, try again.'));
                $this->Flash->set(
                    __('The password could not be saved. Please, try again.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-danger'
                        ]
                    ]
                );
            }             
        }
        
        $this->set(compact('user'));
    }
            
    /**
     * update password
     * @return type
     */
    public function updatePassword($id = null)
    {
        //user access check
        $session = $this->request->session();
        if ($session->read('user_type') != 'client' || $this->Auth->user('id') != $id) {       
            $this->Flash->set(
                __('Access Denied!'), 
                ['params' => 
                    [
                        'class' => 'alert alert-danger'
                    ]
                ]
            );
            $this->redirect(['controller' => 'dashboard', 'action' => 'client']);
        }
        // end of user access check
        
        $user = $this->Clients->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            //$user = $this->Users->patchEntity($user, $this->request->data);
            
            $user = $this->Clients->patchEntity($user, [
                    'old_password' => $this->request->data['old_password'],
                    'password' => $this->request->data['password'],
                    'confirm_password' => $this->request->data['confirm_password']
                ]
            );
            
            if ($this->Clients->save($user)) {
                //$this->Flash->success(__('The password has been saved.'));
                $this->Flash->set(
                    __('The password has been saved.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-success'
                        ]
                    ]
                );
                //return $this->redirect(['action' => 'index']);
            } else {
                //$this->Flash->error(__('The password could not be saved. Please, try again.'));
                $this->Flash->set(
                    __('The password could not be saved. Please, try again.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-danger'
                        ]
                    ]
                );
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);        
    }    
    
}
