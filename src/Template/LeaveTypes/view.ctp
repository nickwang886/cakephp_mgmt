<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Leave Type'), ['action' => 'edit', $leaveType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Leave Type'), ['action' => 'delete', $leaveType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $leaveType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Leave Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Leave Type'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Calendars'), ['controller' => 'Calendars', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Calendar'), ['controller' => 'Calendars', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="leaveTypes view large-9 medium-8 columns content">
    <h3><?= h($leaveType->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($leaveType->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($leaveType->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($leaveType->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($leaveType->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Calendars') ?></h4>
        <?php if (!empty($leaveType->calendars)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Leave Type Id') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Start Date') ?></th>
                <th><?= __('End Date') ?></th>
                <th><?= __('Total Days') ?></th>
                <th><?= __('Repeating') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($leaveType->calendars as $calendars): ?>
            <tr>
                <td><?= h($calendars->id) ?></td>
                <td><?= h($calendars->leave_type_id) ?></td>
                <td><?= h($calendars->title) ?></td>
                <td><?= h($calendars->description) ?></td>
                <td><?= h($calendars->start_date) ?></td>
                <td><?= h($calendars->end_date) ?></td>
                <td><?= h($calendars->total_days) ?></td>
                <td><?= h($calendars->repeating) ?></td>
                <td><?= h($calendars->created) ?></td>
                <td><?= h($calendars->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Calendars', 'action' => 'view', $calendars->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Calendars', 'action' => 'edit', $calendars->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Calendars', 'action' => 'delete', $calendars->id], ['confirm' => __('Are you sure you want to delete # {0}?', $calendars->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
