<ol class="breadcrumb" style="margin-bottom: 5px;">
    <li>
        <?php
        echo $this->Html->link(
                __('Dashboard'), ['controller' => 'dashboard', 'action' => 'index']
        );
        ?>
    </li>
    <li>
        <?php
        echo $this->Html->link(
                __('Users'), ['controller' => 'users', 'action' => 'index']
        );
        ?>
    </li>
    <li class="active"><?= __('View') ?></li>
</ol>

<div class="users form col-md-12">
    <span class="title-edit text-center" style="margin: 10px 0px;"><?= __('User Information') ?></span>
    <table class="table table-bordered white_bg">
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= $user->email ?></td>
        </tr>
        <tr>
            <th><?= __('First Name') ?></th>
            <td><?= $user->first_name ?></td>
        </tr>
        <tr>
            <th><?= __('Last Name') ?></th>
            <td><?= $user->last_name ?></td>
        </tr>
        <tr>
            <th><?= __('Contact Number') ?></th>
            <td><?= $user->contact_number ?></td>
        </tr>
        <tr>
            <th><?= __('Leave Balance') ?></th>
            <td class="text-center">
                <?php
                $user_leave_count = 0;

                if (!empty($user_leaves)) {
                    foreach ($user_leaves as $user_leave) {
                        echo $user_leave->leave_type['name'] . ' : ' . $user_leave->balance . '<br />';

                        $user_leave_count += $user_leave->balance;
                    }
                }

                echo '<div style="width: 100%; border-top: 1px solid black;">' . __('Total : ') . $user_leave_count . '</div>';
                ?>
            </td>
        </tr>
        <tr>
            <th><?= __('Access Level') ?></th>
            <td><?= $this->cell('GetAccessLevelName', [$user->access_level]) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= date_format($user->created, 'Y-m-d H:i:s') ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= date_format($user->modified, 'Y-m-d H:i:s') ?></td>
        </tr>
    </table>
</div>
<div class="related col-md-12">
    <span class="title-related text-center" style="margin: 10px 0px;"><?= __('Leave Applications') ?></span>
    <?php if (!empty($user->calendars)): ?>
        <table class="table table-bordered white_bg">
            <tr>
                <th><?= __('Type') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Start Date') ?></th>
                <th><?= __('End Date') ?></th>
                <th><?= __('Total Days') ?></th>
            </tr>
            <?php foreach ($user->calendars as $calendar): ?>
                <tr>
                    <td><?= $calendar->leave_type['name'] ?></td>
                    <td><?= $calendar->title ?></td>
                    <td><?= $calendar->description ?></td>
                    <td><?= $calendar->start_date ?></td>
                    <td><?= $calendar->end_date ?></td>
                    <td><?= $calendar->total_days ?></td>            
                </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
</div>
