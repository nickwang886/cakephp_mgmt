<?php
namespace App\View\Cell;

use Cake\View\Cell;

/**
 * GetAllStaff cell
 */
class GetAllStaffCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
    }
    
    /**
     * all staff options method.
     *
     * @return void
     */
    public function options()
    {
        // get all staff 
        $this->loadModel('Users');
        $staff = $this->Users->find()
                ->where(['Users.access_level >' => 1])
                ->order(['Users.first_name' => 'ASC']);
        
        $this->set(compact('staff'));
    }
    
}
