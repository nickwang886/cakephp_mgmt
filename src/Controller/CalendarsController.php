<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Calendars Controller
 *
 * @property \App\Model\Table\CalendarsTable $Calendars
 */
class CalendarsController extends AppController
{                    
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {                    
        // get all events within the year for specified user
        if ($this->Auth->user('access_level') < 3) { 
            // get all events within the year for the logged in user
            $calendars = $this->Calendars->find('all', [
                'contain' => ['Users', 'LeaveTypes']
            ]);       
        } 
        else {
            $user_id = $this->Auth->user('id');
            
            $calendars = $this->Calendars->find('all', [
                'contain' => ['Users', 'LeaveTypes']
            ])
            ->matching('Users', function ($q) use ($user_id) {
                return $q->where(['Users.id' => $user_id]);
            });             
        }
                          
        $this->set(compact('calendars'));
        
        // find users
        $this->loadModel('Users');
        $staff_list = $this->Users->find('staff_list');
            
        $this->set(compact('staff_list'));                
    }

    /**
     * View method
     *
     * @param string|null $id Calendar id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $calendars = $this->Calendars->find('all', [
            'contain' => ['Users', 'LeaveTypes']
        ]); 
        
        $this->set('calendars', $calendars);
        $this->set('_serialize', ['calendars']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {                
        $calendar = $this->Calendars->newEntity();
        
        if ($this->request->is('post')) {               
            // verify user leaves
            if (!empty($this->request->data['staff_ids'])) {                
                foreach ($this->request->data['staff_ids'] as $key => $value) {                   
                    // user leaves
                    $this->loadModel('UserLeaves');
                   
                    $user_leave = $this->UserLeaves->find()
                            ->where([
                                'UserLeaves.user_id' => $value,
                                'UserLeaves.leave_type_id' => $this->request->data['leave_type_id']
                            ])
                            ->first();

                    if (empty($user_leave)) {                        
                        $this->Flash->set(
                            __('One or more users do not entitle the selected leave type. Please, try again.'), 
                            ['params' => 
                                [
                                    'class' => 'alert alert-danger'
                                ]
                            ]
                        );
                        
                        return $this->redirect($this->referer());                        
                    }   
                }
            }                    
            
            if (!empty($this->request->data['title'])) {
                switch ($this->request->data['title']) {
                    case 'full_day':
                        $this->request->data['title'] = 'Full-day';
                        $this->request->data['start_date'] = $this->request->data['start_date'].' 09:00:00';
                        $this->request->data['end_date'] = $this->request->data['end_date'].' 18:00:00';                        
                        break;
                    case 'half_day_am':
                        $this->request->data['title'] = 'Half-day (AM)';
                        $this->request->data['start_date'] = $this->request->data['start_date'].' 09:00:00';
                        $this->request->data['end_date'] = $this->request->data['end_date'].' 13:00:00';                        
                        break;
                    case 'half_day_pm':
                        $this->request->data['title'] = 'Half-day (PM)';
                        $this->request->data['start_date'] = $this->request->data['start_date'].' 14:00:00';
                        $this->request->data['end_date'] = $this->request->data['end_date'].' 18:00:00';                        
                        break;
                    default:
                        $this->request->data['title'] = 'Full-day';
                        $this->request->data['start_date'] = $this->request->data['start_date'].' 09:00:00';
                        $this->request->data['end_date'] = $this->request->data['end_date'].' 18:00:00';                        
                        break;
                }
            }
            
            $calendar = $this->Calendars->patchEntity($calendar, $this->request->data);
            
            if ($this->Calendars->save($calendar)) {                
                // related staff leave balance
                if (!empty($this->request->data['staff_ids'])) {
                    $calendarsUsersTable = TableRegistry::get('CalendarsUsers');
                                                          
                    foreach ($this->request->data['staff_ids'] as $key => $value) {
                        $calendarsUser = $calendarsUsersTable->newEntity();
                    
                        $calendarsUser->calendar_id = $calendar->id;
                        $calendarsUser->user_id = $value;
                        
                        if ($calendarsUsersTable->save($calendarsUser)) {
                            // leave balance
                            $this->loadModel('UserLeaves');
                                                        
                            $user_leave = $this->UserLeaves->find()
                                    ->where([
                                        'UserLeaves.user_id' => $value,
                                        'UserLeaves.leave_type_id' => $this->request->data['leave_type_id']
                                    ])
                                    ->first();
                            
                            if (!empty($user_leave)) {
                                $updated_user_leave_balance = $user_leave->balance - $calendar->total_days;
                                
                                $user_leave->balance = $updated_user_leave_balance;
                                
                                $this->UserLeaves->save($user_leave);
                            }
                        }
                    }                    
                }            
                
                //$this->Flash->success(__('The calendar has been saved.'));
                $this->Flash->set(
                    __('The calendar has been saved.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-success'
                        ]
                    ]
                );                                
            } 
            else {
                //$this->Flash->error(__('The calendar could not be saved. Please, try again.'));                
                $this->Flash->set(
                    __('The calendar could not be saved. Please, try again.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-danger'
                        ]
                    ]
                );
            }
            
            return $this->redirect(['action' => 'index']);
        }
        
        $this->set(compact('calendar'));
        $this->set('_serialize', ['calendar']);
        
        // leave types
        $this->loadModel('LeaveTypes');
        $leave_types = $this->LeaveTypes->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ]);
            
        $this->set(compact('leave_types'));
        
        // logged in user         
        $this->loadModel('Users');
        
        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => [
                'UserLeaves' => [
                    'LeaveTypes'
                ]
            ]
        ]);
        
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Calendar id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $calendar = $this->Calendars->get($id, [
            'contain' => ['Users', 'LeaveTypes']
        ]);
        
        if ($this->request->is(['patch', 'post', 'put'])) {   
            
            $calendar = $this->Calendars->patchEntity($calendar, $this->request->data);
            
            if ($this->Calendars->save($calendar)) {                                                                               
                //$this->Flash->success(__('The calendar has been saved.'));
                $this->Flash->set(
                    __('The calendar has been saved.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-success'
                        ]
                    ]
                );                       
            } 
            else {
                //$this->Flash->error(__('The calendar could not be saved. Please, try again.'));
                $this->Flash->set(
                    __('The calendar could not be saved. Please, try again.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-danger'
                        ]
                    ]
                );        
            }
            
            return $this->redirect(['action' => 'index']);
        }
                        
        $this->set(compact('calendar'));
        $this->set('_serialize', ['calendar']);                        
    }

    /**
     * Delete method
     *
     * @param string|null $id Calendar id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        
        $calendar = $this->Calendars->get($id);
        
        // leave balance
        $users = $this->Calendars->Users->find('list', [
            'valueFeild' => 'id'
        ])
        ->matching('CalendarsUsers', function ($q) use ($id) {
            return $q->where(['CalendarsUsers.calendar_id' => $id]);
        });

        if (!empty($users)) {
            $this->loadModel('UserLeaves');

            foreach ($users as $key => $value) {                         
                $user_leave = $this->UserLeaves->find()
                        ->where([
                            'user_id' => $value,
                            'leave_type_id' => $calendar->leave_type_id
                        ])
                        ->first();

                if (!empty($user_leave)) {
                    $updated_user_leave_balance = $user_leave->balance + $calendar->total_days;
                    $user_leave->balance = $updated_user_leave_balance;

                    $this->UserLeaves->save($user_leave);
                } 
                else {
                    $this->Flash->set(
                        __('One or more users do not entitle the selected leave type. Please, try again.'), 
                        ['params' => 
                            [
                                'class' => 'alert alert-danger'
                            ]
                        ]
                    ); 
                    
                    return $this->redirect($this->referer());
                }
            }
        }
                         
        if ($this->Calendars->delete($calendar)) {                              
            //$this->Flash->success(__('The calendar has been deleted.'));
            $this->Flash->set(
                __('The calendar has been deleted.'), 
                ['params' => 
                    [
                        'class' => 'alert alert-success'
                    ]
                ]
            );      
        } 
        else {
            //$this->Flash->error(__('The calendar could not be deleted. Please, try again.'));
            $this->Flash->set(
                __('The calendar could not be deleted. Please, try again.'), 
                ['params' => 
                    [
                        'class' => 'alert alert-danger'
                    ]
                ]
            );      
        }
        
        return $this->redirect($this->referer());
    }
    
    /**
     * by user
     *
     * @return \Cake\Network\Response|null
     */
    public function byUser($user_id = null)
    {   
        // user access check        
        $session = $this->request->session();
        
        if (($session->read('user_type') != 'user') || $this->Auth->user('access_level') > 2) {       
            $this->Flash->set(
                __('Access Denied!'), 
                ['params' => 
                    [
                        'class' => 'alert alert-danger'
                    ]
                ]
            );

            $this->redirect(['controller' => 'dashboard', 'action' => 'index']);
        }                        
        // user access check
        
        if (!empty($user_id)) {
            // get all events within the year for specified user
            $calendars = $this->Calendars->find('all', [
                'contain' => ['Users']
            ])
            ->matching('Users', function ($q) use ($user_id) {
                return $q->where(['Users.id' => $user_id]);
            });                                
        } 
        else {
            // get all events within the year
            $calendars = $this->Calendars->find('all', [
                'contain' => ['Users', 'LeaveTypes']
            ]);         
        }
        
        $this->set('user_id', $user_id);
        $this->set(compact('calendars'));
        
        $this->loadModel('Users');
        $staff_list = $this->Users->find('staff_list');
            
        $this->set(compact('staff_list'));
    }
    
}
