<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * LeaveTypes Controller
 *
 * @property \App\Model\Table\LeaveTypesTable $LeaveTypes
 */
class LeaveTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $leaveTypes = $this->paginate($this->LeaveTypes);

        $this->set(compact('leaveTypes'));
        $this->set('_serialize', ['leaveTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Leave Type id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $leaveType = $this->LeaveTypes->get($id, [
            'contain' => ['Calendars']
        ]);

        $this->set('leaveType', $leaveType);
        $this->set('_serialize', ['leaveType']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $leaveType = $this->LeaveTypes->newEntity();
        
        if ($this->request->is('post')) {
            $leaveType = $this->LeaveTypes->patchEntity($leaveType, $this->request->data);
            
            if ($this->LeaveTypes->save($leaveType)) {
                //$this->Flash->success(__('The leave type has been saved.'));                
                $this->Flash->set(
                    __('The leave type has been saved.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-success'
                        ]
                    ]
                );   
            } 
            else {
                //$this->Flash->error(__('The leave type could not be saved. Please, try again.'));
                $this->Flash->set(
                    __('The leave type could not be saved. Please, try again.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-danger'
                        ]
                    ]
                );        
            }
            
            return $this->redirect($this->referer());
        }
        
        $this->set(compact('leaveType'));
        $this->set('_serialize', ['leaveType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Leave Type id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $leaveType = $this->LeaveTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $leaveType = $this->LeaveTypes->patchEntity($leaveType, $this->request->data);
            if ($this->LeaveTypes->save($leaveType)) {
                $this->Flash->success(__('The leave type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The leave type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('leaveType'));
        $this->set('_serialize', ['leaveType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Leave Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $leaveType = $this->LeaveTypes->get($id);
        if ($this->LeaveTypes->delete($leaveType)) {
            $this->Flash->success(__('The leave type has been deleted.'));
        } else {
            $this->Flash->error(__('The leave type could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    /**
     * manage leave types
     */
    public function manage()
    {
        $leave_types = $this->LeaveTypes->find();
        
        if ($this->request->is('post')) {          
            $action_flag = false;                
     
            // update leave types
            if (!empty($this->request->data['edit_leave_types'])) {  
                foreach ($this->request->data['edit_leave_types'] as $key => $value) {
                    $leave_type = $this->LeaveTypes->get($value);
                    
                    if (!empty($leave_type)) {
                        $leave_type->name = $this->request->data['leave_type_name'][$value];
                        $leave_type->color_code = $this->request->data['leave_type_color_code'][$value];
                                                                        
                        if ($this->LeaveTypes->save($leave_type)) {                            
                            $action_flag = true;
                        }
                    }
                }
            }
            
            // delete leave types
            if (!empty($this->request->data['delete_leave_types'])) {  
                foreach ($this->request->data['delete_leave_types'] as $key => $value) {
                    $leave_type = $this->LeaveTypes->get($value);
                    
                    if (!empty($leave_type)) {
                        if ($this->LeaveTypes->delete($leave_type)) {
                            $action_flag = true;
                        }
                    }
                }
            }                        
            
            if ($action_flag) {
                $this->Flash->set(
                    __('The leave type has been saved.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-success'
                        ]
                    ]
                );   
            } 
            else {
                $this->Flash->set(
                    __('The leave type could not be saved. Please, try again.'), 
                    ['params' => 
                        [
                            'class' => 'alert alert-danger'
                        ]
                    ]
                );   
            }
            
            return $this->redirect($this->referer());
        }
        
        $this->set(compact('leave_types'));
    }
}
