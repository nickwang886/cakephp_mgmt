<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ajax Controller
 */
class AjaxController extends AppController
{
    var $uses = false;
    
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }
    
}
