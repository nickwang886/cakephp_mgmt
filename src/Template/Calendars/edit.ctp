<div class="calendars form">
    <?php
    echo $this->Form->create($calendar, [
        'type' => 'file',
        'templates' => $template_form_48
    ]);
    ?>

    <div class="modal-header modal-header-primary">       
        <button class="close" data-dismiss="modal">&times;</button>
        <b><?php echo __('Calendar'); ?></b>
    </div>

    <div class="modal-body">
        <fieldset>
            <?php
            echo '<div class="form-group clearfix">';
            echo '<label class="col-md-4 control-label">' . __('Leave Type') . '</label>';
            echo '<div class="col-md-8">';
            echo '<pre class="input-sm">' . $calendar->leave_type['name'] . '</pre>';
            echo '</div>';
            echo '</div>';

            echo '<div class="form-group clearfix">';
            echo '<label class="col-md-4 control-label">' . __('Title') . '</label>';
            echo '<div class="col-md-8">';
            echo '<pre class="input-sm">' . $calendar->title . '</pre>';
            echo '</div>';
            echo '</div>';

            // description
            echo '<div class="form-group clearfix">';
            echo '<label class="col-md-4 control-label">' . __('Description') . '</label>';
            echo '<div class="col-md-8">';
            echo $this->Form->input('description', [
                'label' => false,
                'type' => 'textarea',
                'class' => 'form-control',
                'style' => 'min-width: 100%',
                'rows' => 3,
                'value' => $calendar->description
            ]);
            echo '</div>';
            echo '</div>';

            echo '<div class="form-group clearfix">';
            echo '<label class="col-md-4 control-label">' . __('Start Date') . '</label>';
            echo '<div class="col-md-8">';
            echo '<pre class="input-sm">' . $calendar->start_date . '</pre>';
            echo '</div>';
            echo '</div>';

            echo '<div class="form-group clearfix">';
            echo '<label class="col-md-4 control-label">' . __('End Date') . '</label>';
            echo '<div class="col-md-8">';
            echo '<pre class="input-sm">' . $calendar->end_date . '</pre>';
            echo '</div>';
            echo '</div>';

            echo '<div class="col-md-4"></div>'
            . '<div class="col-md-8"><small class="custom_info_message">&#x26a0; ' . __('Full-day') . '  = 1, ' . __('Half-day') . '  = 0.5</small></div>';

            echo '<div class="form-group clearfix">';
            echo '<label class="col-md-4 control-label">' . __('Total Days') . '</label>';
            echo '<div class="col-md-8">';
            echo '<pre class="input-sm">' . $calendar->total_days . '</pre>';
            echo '</div>';
            echo '</div>';

            // realted staff                
            if (!empty($calendar->users)) {
                echo '<div class="form-group clearfix">';
                echo '<label class="col-md-4 control-label">' . __('Staff') . '</label>';
                echo '<div class="col-md-8">';
                foreach ($calendar->users as $staff) {
                    echo '<i class="fa fa-user" aria-hidden="true"></i> ' . $staff->first_name . ' ' . $staff->last_name . ' (' . $staff->email . ')<br />';
                    echo '<input type="hidden" name="staff_ids[]" value="' . $staff->id . '" />';
                }
                echo '</div>';
                echo '</div>';
            }
            ?>
        </fieldset>
    </div>

    <div class="modal-footer">
        <?php        
            echo $this->Form->button(__('Save'), ['class' => 'btn btn-primary pull-right']);
            echo '<button class="btn btn-default btn_margin_right_5px" data-dismiss="modal">'.__('Close').'</button>';
        ?>        
        <?= $this->Form->end() ?> 
        <?php
        if ($this->request->session()->read('Auth.User.access_level') < 3) {
            echo $this->Form->postLink(__('Delete'), ['controller' => 'calendars', 'action' => 'delete', $calendar->id], [
                'class' => 'btn btn-danger pull-right btn_margin_right_5px',
                'confirm' => __('Confirm to delete?')
            ]);
        }
        ?>        
    </div>
</div>