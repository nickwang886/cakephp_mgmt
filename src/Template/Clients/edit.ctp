<div class="clients form col-md-12 ">
    <?= $this->Form->create($client, [
            'templates' => $template_form_48
        ]) 
    ?>
    <fieldset>
        <legend><?= __('Edit Profile') ?></legend>
        <?php
            echo $this->Form->input('unique_code');
            echo $this->Form->input('email');
            echo $this->Form->input('password');
            echo $this->Form->input('salutation');
            echo $this->Form->input('first_name');
            echo $this->Form->input('last_name');
            
            echo '<div class="form-group clearfix">';
            echo '<label class="col-md-4 control-label" for="title">Date of Birth</label>';
            echo '<div id="birthdayPicker" class="col-md-8">';
            echo '</div>';
            echo '</div>';
                
            echo $this->Form->input('gender', [
                'options' => [
                    'Male' => 'Male',
                    'Female' => 'Female'
                ],
                'empty' => '(choose one)'
            ]);
            echo $this->Form->input('mobile_number');
            echo $this->Form->input('residential_number');
            echo $this->Form->input('address_street');
            echo $this->Form->input('address_building');
            echo $this->Form->input('address_unit');
            echo $this->Form->input('address_postal_code');
        ?>
        
        <div class="col-md-12">        
            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary pull-right']); ?>
            <?= $this->Form->end(); ?>
        </div>        
    </fieldset>    
</div>

<script type="text/javascript">
    $("#birthdayPicker").birthdayPicker();
</script>
