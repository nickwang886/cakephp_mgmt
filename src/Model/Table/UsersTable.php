<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Calendars
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Calendars', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'calendar_id',
            'joinTable' => 'calendars_users'
        ]);
        $this->hasMany('UserLeaves', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->allowEmpty('first_name');

        $validator
            ->allowEmpty('last_name');

        $validator
            ->allowEmpty('contact_number');
        
        $validator
            ->integer('access_level')
            ->requirePresence('access_level', 'create')
            ->notEmpty('access_level');
        
        $validator
            ->add('confirm_password',
                'compareWith', [
                    'rule' => ['compareWith', 'password'],
                    'message' => 'Passwords do not match'
                ]
            );
        
        $validator
            ->add('old_password','custom',[
                'rule' => function($value, $context){
                    $user = $this->get($context['data']['id']);
                    if ($user) {
                        if ((new DefaultPasswordHasher)->check($value, $user->password)) {
                            return true;
                        }
                    }
                    return false;
                },
                'message'=>'The old password does not match the current password',
            ])
            ->notEmpty('old_password');        

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        return $rules;
    }
    
    /**
     * find by access level     
     */
    public function findByAccessLevel(Query $query, array $access_level){
        return $query
            ->where($access_level);
    }
    
    /**
     * find staff list     
     */
    public function findStaff_list()
    {
        return $this->find()
            ->select(['id', 'email', 'first_name', 'last_name'])
            ->where(['Users.access_level >' => 2])
            ->formatResults(function($results) {
                return $results->combine(
                    'id',
                    function($row) {
                        return $row['first_name'] . ' ' . $row['last_name'] . ' (' . $row['email'] . ')';
                    }
                );
            })
            ->order(['Users.first_name' => 'ASC']);
    }
}
