<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

//$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <?php
        $site_title = '';
        switch ($this->request->params['action']) {             
            default: 
                $site_title = 'Client';
                break;
        }
    ?>
    
    <title><?=$site_title?></title>
    <?= $this->Html->meta('icon') ?>

    <!-- css -->
    <?php //echo $this->Html->css('base.css'); ?>
    <?php //echo $this->Html->css('cake.css'); ?>
    
    <?php echo $this->Html->css('font-awesome.min'); ?>
    <?php echo $this->Html->css('bootstrap.min'); ?>
    <?php echo $this->Html->css('bootstrap-theme.min'); ?>        
    <?php echo $this->Html->css('bootstrap-datetimepicker.min'); ?>
    
    <!-- datatables -->
    <?php echo $this->Html->css('datatables/datatables.min'); ?>
    <?php echo $this->Html->css('datatables/buttons.dataTables.min'); ?>
    <?php echo $this->Html->css('datatables/rowReorder.bootstrap.min'); ?>
    <?php echo $this->Html->css('datatables/rowReorder.dataTables.min'); ?>
    <?php echo $this->Html->css('datatables/rowReorder.foundation.min'); ?>
    <?php echo $this->Html->css('datatables/rowReorder.jqueryui.min'); ?>
    <!-- end of css -->   
    
    <!-- javascript -->
    <?php echo $this->Html->script('jquery-1.12.3.min'); ?>
    <?php echo $this->Html->script('moment'); ?>
    <?php echo $this->Html->script('bootstrap.min'); ?>
    <?php echo $this->Html->script('bootstrap-datetimepicker.min'); ?> 
    <?php echo $this->Html->script('ckeditor/ckeditor'); ?>
    <?php echo $this->Html->script('jquery-birthday-picker'); ?>
    
    <!-- datatables -->
    <?php echo $this->Html->script('datatables/datatables.min'); ?>
    <?php echo $this->Html->script('datatables/dataTables.buttons.min'); ?>
    <?php echo $this->Html->script('datatables/buttons.flash.min'); ?>
    <?php echo $this->Html->script('datatables/buttons.html5.min'); ?>
    <?php echo $this->Html->script('datatables/buttons.print.min'); ?>    
    <?php echo $this->Html->script('datatables/jszip.min'); ?>
    <?php echo $this->Html->script('datatables/pdfmake.min'); ?>
    <?php echo $this->Html->script('datatables/vfs_fonts'); ?>
    <?php echo $this->Html->script('datatables/dataTables.rowReorder.min'); ?>
    <!-- end of javascript -->
    
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>          
    <style>
        body, .modal-body, .modal-footer {
            background-image: url(<?=$this->Url->image('/img/site_bg_client.jpg');?>);
            background-repeat: repeat;
        }
        
        footer {
            background: #000;
            color: #fff;
        }
                        
        /* bread crumb */
        .breadcrumb a:hover {
            text-decoration: none;
        }
        
        .breadcrumb > li + li:before {
            color: #ccc;
            content: "> ";
            padding: 0 5px;
        }
        /* end */                        
        
        /* datetable */ 
        .dataTables_length select {
            width: 52px;
        }
        
        table.dataTable thead th {
            background-color: #ECECEA;            
        }
        
        table.dataTable tbody td {
            vertical-align: middle;
        }
        
        table.dataTable thead th .dnd { 
            color: #f0ad4e;
        }
                
        div.dt-buttons {
            margin: 0px 20px;
        }        
        /* end */
        
        /* required field mark */
        .form-group.required .control-label:after { 
            content: "*";
            color: red;
        }
        /* end */
        
        /* makes all input selection 100% width */
        select {
            width: 100%;
        }
        /* end */
        
        /* buttons related */
        .btn_margin_right_5px {
            margin-right: 5px;
        }
        
        .btn-xs {
            margin-bottom: 5px;
        }               
        
        .btn_font_icon {
            width: 30px;
        }                
        /* end of buttons related */
        
        /* form */     
        legend {
            text-align: center;
        }
        
        .form label {
            background: #ECECEA;
            border-right: 1px dotted grey;
            border-top-left-radius: 50px;
            border-bottom-left-radius: 50px;
            line-height: 30px;            
        }
        
        .form .form-group {
            margin-bottom: 0px;
        }
        /* end */
        
        /* custom message */
        .message {
            position: absolute !important;            
            right: 1% !important;
            border-width: 5px !important;
            z-index: 1 !important;
        }
        
        .custom_info_message {
            color: red;
        }
        /* end of custom message */       
                        
        /* bootstrap modal */
        .modal-body, .modal-footer {
            background: #ECECEA;
        }
        
        .modal-header-success {
            color:#fff;
            padding:9px 15px;
            border-bottom:1px solid #eee;
            background-color: #5cb85c;
        }
        
        .modal-header-warning {
            color:#fff;
            padding:9px 15px;
            border-bottom:1px solid #eee;
            background-color: #f0ad4e;
        }
        
        .modal-header-danger {
            color:#fff;
            padding:9px 15px;
            border-bottom:1px solid #eee;
            background-color: #d9534f;
        }
        
        .modal-header-info {
            color:#fff;
            padding:9px 15px;
            border-bottom:1px solid #eee;
            background-color: #5bc0de;
        }
        
        .modal-header-primary {
            color:#fff;
            padding:9px 15px;
            border-bottom:1px solid #eee;
            background-color: #428bca;            
        }
        /* end of bootstrap modal header */
                       
        #div_actions, #div_filter {
            display: block;
            background: #fff;
            padding: 10px;
            margin-bottom: 20px;
            border: 1px solid lightgray;
            box-shadow: 3px 3px 3px lightgray;
            overflow: auto;
        }        
    </style>
</head>

<body>   
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="background: #fff;">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">                    
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php
                    echo $this->Html->link(
                        '<img src="' . $this->request->webroot . 'img/nav_brand.png" height="30px;">', 
                        ['controller' => 'dashboard', 'action' => 'client'], 
                        [
                            'class' => 'navbar-brand',
                            'escape' => false
                        ]
                    );
                ?>
            </div>
            
            <?php if (!empty($this->request->session()->read('Auth.User.id'))) { ?>
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <?php
                            echo $this->Html->link(
                                __('Logout'), 
                                ['controller' => 'clients', 'action' => 'logout'], 
                                [
                                    'class' => 'btn btn-sm btn-warning pull-right', 
                                    'style' => 'margin-right: 5px; margin-top: 10px;',
                                    'escape'=>false
                                ]
                            );                                                          
                        ?>
                    </ul>
                    
                    <ul class="nav navbar-nav navbar-right">                                                        
                        <?php 
                            if ($this->request->params['controller'] == 'Clients') { 
                        ?> 
                        <li class="dropdown active">
                        <?php 
                            } 
                            else {
                                echo '<li class="dropdown">';
                            }
                        ?>           
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="fa fa-user" aria-hidden="true"></i> <?=__('Profile')?>
                            </a>
                            
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <?php 
                                        echo $this->Html->link(
                                            '<i class="fa fa-caret-right" aria-hidden="true"></i> '.__('View Profile'), 
                                            [
                                                'controller' => 'clients', 
                                                'action' => 'edit', $this->request->session()->read('Auth.User.id')
                                            ], 
                                            [
                                                'escape' => false
                                            ]
                                        ); 
                                    ?>
                                </li>

                                <li>                        
                                    <?php 
                                        echo $this->Html->link(
                                            '<i class="fa fa-caret-right" aria-hidden="true"></i> '.__('Change password'), 
                                            [
                                                'controller' => 'clients', 
                                                'action' => 'update_password/'.$this->request->session()->read('Auth.User.id')
                                            ], 
                                            [
                                                'escape' => false
                                            ]
                                        ); 
                                    ?>
                                </li>    
                            </ul>
                        </li>                                                      
                    </ul>                                        
                </div>
            <?php } ?>
        </div>        
    </nav>
    
    <div class="container clearfix" style="margin-bottom: 40px;">
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
    </div>
    
    <!--
    <footer class="footer navbar-fixed-bottom">
        <div class="container">
            <div class="text-center">
                Copyright &copy; <?=date('Y')?>
            </div>
        </div>
    </footer>
    -->
    
    <script type="text/javascript">
        $(document).ready(function () {
            // fade out flash messages
            $('.message').delay(3000).fadeOut(300, function() {
                $(".message").remove();
            });        

            // clear Bootstrap Modal content after close
            $('body').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
            });
        });
    </script>
</body>
</html>