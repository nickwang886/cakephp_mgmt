<div id="div_actions">
    <?php
    if ($this->request->session()->read('Auth.User.access_level') < 3) {
        echo '<button class="btn btn-sm btn-info btn_margin_right_5px" data-toggle="modal" data-target="#add_leave_type" data-remote="' . $this->Url->build(['controller' => 'leave_types', 'action' => 'add']) . '">' . __('Add Leave Type') . '</button>';

        echo '<button class="btn btn-sm btn-info btn_margin_right_5px" data-toggle="modal" data-target="#manage_leave_types" data-remote="' . $this->Url->build(['controller' => 'leave_types', 'action' => 'manage']) . '">' . __('Manage Leave Types') . '</button>';

        echo '<button class="btn btn-sm btn-primary btn_margin_right_5px" data-toggle="modal" data-target="#add_user" data-remote="' . $this->Url->build(['controller' => 'users', 'action' => 'add']) . '">' . __('Add User') . '</button>';
    }

    echo $this->Html->link(
            __('Change Password'), [
        'controller' => 'users',
        'action' => 'update_password/' . $this->request->session()->read('Auth.User.id')
            ], [
        'class' => 'btn btn-sm btn-primary'
            ]
    );
    ?>
</div>

<div id="div_data_table">    
    <table id="tbl_users" class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th><?= __('Email') ?></th>
                <th><?= __('First Name') ?></th>
                <th><?= __('Last Name') ?></th>
                <th><?= __('Contact Number') ?></th>
                <th><?= __('Leave Balance') ?></th>
                <th><?= __('Access Level') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
                <tr>
                    <td><?= $user->email ?></td>
                    <td><?= $user->first_name ?></td>  
                    <td><?= $user->last_name ?></td>  
                    <td><?= $user->contact_number ?></td>  
                    <td class="text-center">
                    <?php
                    $user_leave_count = 0;
                    
                    if (!empty($user->user_leaves)) {
                        foreach ($user->user_leaves as $user_leave) {
                            $user_leave_count += $user_leave->balance;
                        }
                    }                    
                    echo $user_leave_count;
                    
                    echo $this->Html->link(
                        '<i class="fa fa-list" aria-hidden="true"></i>', 
                        [
                            'controller' => 'users',
                            'action' => 'view', $user->id
                        ], 
                        [
                            'class' => 'btn btn-xs btn-default pull-right',
                            'escape' => false
                        ]
                    );
                    ?>
                    </td>  
                    <td><?= $this->cell('GetAccessLevelName', [$user->access_level]) ?></td>  
                    <td class="actions">
                        <?php
                        echo '<button class="btn btn-xs btn-primary" data-toggle="modal" data-target="#edit_user" data-remote="' . $this->Url->build(['controller' => 'users', 'action' => 'edit', $user->id]) . '">' . __('Edit') . '</button>';

                        if ($this->request->session()->read('Auth.User.access_level') < 3 && $user->access_level > 2) {
                            echo '&nbsp;' . $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['class' => 'btn btn-xs btn-danger', 'confirm' => __('Confirm to delete?')]);
                        }
                        ?>
                    </td>                
                </tr>
                <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div id="view_user" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <!-- remote content will be inserted here via jQuery load() -->
            </div>
        </div>
    </div>
</div>

<div id="add_user" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <!-- remote content will be inserted here via jQuery load() -->
            </div>
        </div>
    </div>
</div>

<div id="edit_user" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <!-- remote content will be inserted here via jQuery load() -->
            </div>
        </div>
    </div>
</div>

<div id="add_leave_type" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <!-- remote content will be inserted here via jQuery load() -->
            </div>
        </div>
    </div>
</div>

<div id="manage_leave_types" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <!-- remote content will be inserted here via jQuery load() -->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#tbl_users').DataTable({
            stateSave: true,
            order: [[0, "asc"]],
            iDisplayLength: 20,
            aLengthMenu: [[20, 50, 100, -1], [20, 50, 100, "All"]],
            responsive: true,
            dom: '<lBf<t>ip>', // B = button, l = select entries, f = search bokx, i = record info, p = pagination                      
            buttons: [
                'copy',
                {
                    extend: 'csv',
                    title: 'data_export_<?= date('Ymd') ?>'
                },
                {
                    extend: 'excel',
                    title: 'data_export_<?= date('Ymd') ?>'
                },
                {
                    extend: 'pdf',
                    title: 'data_export_<?= date('Ymd') ?>'
                },
                'print'
            ],
            columns: [
                {width: "20%"},
                {width: "15%"},
                {width: "15%"},
                {width: "10%"},
                {width: "10%"},
                {width: "10%"},
                {width: "20%"}
            ]
        });
    });
</script>
