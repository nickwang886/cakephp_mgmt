<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User Leave'), ['action' => 'edit', $userLeave->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User Leave'), ['action' => 'delete', $userLeave->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userLeave->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User Leaves'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Leave'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Leave Types'), ['controller' => 'LeaveTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Leave Type'), ['controller' => 'LeaveTypes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userLeaves view large-9 medium-8 columns content">
    <h3><?= h($userLeave->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $userLeave->has('user') ? $this->Html->link($userLeave->user->id, ['controller' => 'Users', 'action' => 'view', $userLeave->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Leave Type') ?></th>
            <td><?= $userLeave->has('leave_type') ? $this->Html->link($userLeave->leave_type->name, ['controller' => 'LeaveTypes', 'action' => 'view', $userLeave->leave_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($userLeave->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Balance') ?></th>
            <td><?= $this->Number->format($userLeave->balance) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($userLeave->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($userLeave->modified) ?></td>
        </tr>
    </table>
</div>
