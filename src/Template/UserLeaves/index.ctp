<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User Leave'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Leave Types'), ['controller' => 'LeaveTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Leave Type'), ['controller' => 'LeaveTypes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userLeaves index large-9 medium-8 columns content">
    <h3><?= __('User Leaves') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('leave_type_id') ?></th>
                <th><?= $this->Paginator->sort('balance') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userLeaves as $userLeave): ?>
            <tr>
                <td><?= $this->Number->format($userLeave->id) ?></td>
                <td><?= $userLeave->has('user') ? $this->Html->link($userLeave->user->id, ['controller' => 'Users', 'action' => 'view', $userLeave->user->id]) : '' ?></td>
                <td><?= $userLeave->has('leave_type') ? $this->Html->link($userLeave->leave_type->name, ['controller' => 'LeaveTypes', 'action' => 'view', $userLeave->leave_type->id]) : '' ?></td>
                <td><?= $this->Number->format($userLeave->balance) ?></td>
                <td><?= h($userLeave->created) ?></td>
                <td><?= h($userLeave->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $userLeave->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userLeave->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userLeave->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userLeave->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
