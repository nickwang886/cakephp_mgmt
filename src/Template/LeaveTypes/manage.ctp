<div class="projectAwards form model_width_1000px">
    <?= $this->Form->create(null, ['templates' => $template_form_48]) ?>
    
    <div class="modal-header modal-header-primary">       
        <button class="close" data-dismiss="modal">&times;</button>
        <b><?php echo __('Manage Leave Types'); ?></b>
    </div>
    
    <div class="modal-body">
        <fieldset>
            <?php                                 
                if (!empty($leave_types)) {                                                                                
                    echo '<div class="form-group clearfix">';
                    echo '<div class="col-md-2 text-center"><i class="fa fa-times" aria-hidden="true"></i></div>'
                        . '<div class="col-md-2 text-center"><i class="fa fa-pencil" aria-hidden="true"></i></div>'                        
                        . '<div class="col-md-4">'.__('Leave Type Name').'</div>'
                        . '<div class="col-md-4">'.__('Color Code').'</div>';
                    echo '</div>';

                    foreach ($leave_types as $leave_type) {
                        echo '<div class="form-group clearfix">';
                        echo '<div class="col-md-2 text-center"><input type="checkbox" name="delete_leave_types[]" value="'.$leave_type->id.'" /></div>';
                        echo '<div class="col-md-2 text-center"><input type="checkbox" name="edit_leave_types[]" value="'.$leave_type->id.'" /></div>';                                                                                                     
                        echo '<div class="col-md-4"><input type="text" class="form-control input-sm" name="leave_type_name['.$leave_type->id.']" value="'.$leave_type->name.'" /></div>';
                        echo '<div class="col-md-4"><input type="color" class="form-control input-sm" name="leave_type_color_code['.$leave_type->id.']" value="'.$leave_type->color_code.'" /></div>';
                        echo '</div>';
                    }
                } 
                else {
                    echo '<div class="col-md-12 text-center">'.__('No matched record found').'</div>';
                }                                                                                                      
            ?>                
        </fieldset>  
    </div>
    
    <div class="modal-footer">
        <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary pull-right']) ?>
        <button class="btn btn-default btn_margin_right_5px" data-dismiss="modal"><?=__('Close')?></button>
        <?= $this->Form->end() ?> 
    </div>
</div>