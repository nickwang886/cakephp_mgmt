<div class="div_login form col-md-8 col-md-offset-2 columns content">
    <legend><?= __('Client Login') ?></legend>
    <fieldset>
        <?= $this->Form->create(null, ['templates' => $template_form_48]); ?>
        <?= $this->Form->input('email'); ?>
        <?= $this->Form->input('password'); ?>
        
        <div class="col-md-12">
            <?= 
                $this->Html->link(__('Forgot password?'), [
                    'controller' => 'clients', 'action' => 'forgot-password',
                ], [
                    'class' => 'small'
                ]);
            ?>
            <?= $this->Form->button(__('Login'), ['class' => 'btn btn-primary pull-right']); ?>
            <?= $this->Form->end(); ?>
        </div>
    </fieldset>
</div>

<style>
    .div_login {
        background: #fff;        
        padding: 10px;
        border-radius: 10px;
        box-shadow: 3px 3px 3px lightgray;
    }
</style>