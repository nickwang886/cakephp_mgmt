<div class="calendars form">
    <?php
    echo $this->Form->create($leaveType, [
        'templates' => $template_form_48
    ]);
    ?>

    <div class="modal-header modal-header-primary">       
        <button class="close" data-dismiss="modal">&times;</button>
        <b><?php echo __('Add Leave Type'); ?></b>
    </div>

    <div class="modal-body">
        <fieldset>
            <?php
            echo $this->Form->input('name');
            echo $this->Form->input('color_code', [
                'type' => 'color'
            ]);
            ?>
        </fieldset>
    </div>

    <div class="modal-footer">
        <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary pull-right']) ?>
        <button class="btn btn-default btn_margin_right_5px" data-dismiss="modal"><?= __('Close') ?></button>
        <?= $this->Form->end() ?> 
    </div>
</div>
